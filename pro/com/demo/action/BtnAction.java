package com.demo.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.codehaus.jettison.json.JSONArray;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.je.core.action.DynaAction;
import com.je.phone.vo.DsInfoVo;

/**
 * 按钮测试的antion
 * 
 * @author admin
 * @version 2017-05-23 12:31:39
 * @see /DEMO/btnAction!load.action
 */
@Component("btnAction")
@Scope("prototype")
public class BtnAction extends DynaAction {

	private static final long serialVersionUID = 1L;

	/**
	 * 简单输出一个欢迎语
	 */
	public void welcomespeech() {
		toWrite("你好，欢迎使用JEPLUTS。这是后台返回数据！");
	}

	public String actionDemo(DsInfoVo infoVo) {
		JSONObject params = infoVo.getParams();
		String A = params.getString("A");
		JSONArray list = new JSONArray();
		JSONObject map = new JSONObject();
		map.put("A", "a");
		map.put("B", "b");
		list.put(map);
		System.out.println(A);
		return list.toString();
	}

	public int badgeDemo() {
		java.util.Random random = new java.util.Random();// 定义随机类
		return random.nextInt(100);
	};
}