<!-- 跳转登录 单一功能 -->
<!-- 
	跳转链接
	http://localhost:8081/loginForward.jsp?username=admin&password=322&funcCode=testFunc
-->
<%@ page language="java" import="java.util.*"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
	String username = request.getParameter("username");
	String password = request.getParameter("password");
	String funcCode = request.getParameter("funcCode");
	String showView = request.getParameter("showView");//workflow(工作流) formview(功能表单)  空(功能) report(报表)
	String modelID = request.getParameter("modelID");
%>
<!-- jQuery 插件 -->
<script type="text/javascript" src="/JE/resource/jquery/jquery-1.8.3.min.js"></script>
<script type="text/javascript">
  	var isEmpty = function(value){
  		return (value === null) || (value === undefined) || (value === '' ) || (value === 'null');
  	}
  	$.ajax({url:"<%=basePath%>/j_spring_security_check",async:false,
  		data: {
	     j_username:'<%=username%>',
	     j_password:'<%=password%>',
	     j_dept:'default',
	     phone:'1'
     	}});
     var funcCode = "<%=funcCode%>",
     	 showView = "<%=showView%>",
     	 modelID = "<%=modelID%>";
     //iframe特殊操作
     if(showView == 'iframe' && !isEmpty(funcCode)){
     	window.loaction = funcCode;
     }else{
	     var url = '<%=basePath%>/index.jsp?1=1';
	     //功能编码
	     if(!isEmpty(funcCode)){
	     	url += 'funcCode='+funcCode;
	     }
	     //展示形式
	     if(!isEmpty(showView)){
	     	url+="&showView="+showView;
	     }
	     //数据ID
	     if(!isEmpty(modelID)){
	     	url+="&modelID="+modelID;
	     }
	     window.location = url;
     }
     
</script>








